.PHONY: clean

hello: hello.o hello_fn.o
	gcc -o hello hello.o hello_fn.o

hello.o: hello.c
	gcc -c hello.c

hello_fn.o: hello_fn.c
	gcc -c hello_fn.c

clean:
	rm -f *.o hello
